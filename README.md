SolusOS 2 Scripts
-----------------

These scripts are used to test SolusOS 2 from SolusOS 1
They make use of a unionfs tree with a squashfs as a read-only
root system, and make building in a users home directory with
a chroot'd system very easy and light on resource consumption
