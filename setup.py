import os
from setuptools import setup

# Utility function to read the README file.
# Used for the long_description.  It's nice, because now 1) we have a top level
# README file and 2) it's easier to type in the README file than to put a raw
# string in below ...
def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()

setup(
    name = "solusos",
    version = "0.0.1",
    author = "Ikey Doherty",
    author_email = "ikey@solusos.com",
    description = ("SolusOS 2 test system for SolusOS 1 users"),
    license = "GPLv2+",
    keywords = "SolusOS PiSi",
    url = "http://ng.solusos.com",
    packages=['solusos'],
    scripts=['solusos2_shell', 'try_solusos2'],
    long_description=read('README.md'),
)
